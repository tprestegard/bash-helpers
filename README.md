# bash-helpers
A repo providing a set of Bash helper functions, to be imported and used in other scripts.
This is intended to be a simple repo, which can be used as a submodule in other repos which provide Bash scripts.

Sourcing the top-level `all` file will source all other files in this repo.
