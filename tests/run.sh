#! /bin/bash
# Usage: ./run.sh
set -uo pipefail

#########
# Setup #
#########
# Check if prerequisites are installed
PREREQS=( yq )
for CMD in "${PREREQS[@]}"; do
    if ! command -v "${CMD}" &> /dev/null; then
        echo "${CMD} must be installed to run the tests. Exiting..."
        exit 1
    fi
done

# Get paths
REPO_ROOT=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )/.." &> /dev/null && pwd )
SCRIPT_DIR=${REPO_ROOT}/tests
TEST_FILE="${SCRIPT_DIR}/tests.yaml"

# Load tests into array. Each element is a YAML string.
readarray TESTDATA < <(yq e -o=j -I=0 'to_entries | .[]' "${TEST_FILE}")

# Add helpers
. "${REPO_ROOT}/functions/parse_flag"

# Help/usage message
SCRIPT_NAME="$(basename "${0}")"
USAGE="Usage:
  ${SCRIPT_NAME} [OPTIONS]
  ${SCRIPT_NAME} [-f | --function FUNC] [-t | --test TEST]
  ${SCRIPT_NAME} [-h | --help]"

HELP="${SCRIPT_NAME}
  Execute tests for bash helpers provided in this repository.

${USAGE}

Options:
  -f, --function=FUNC       Only execute tests for the function called FUNC.
  -t, --test=TEST           Only execute tests named TEST. Can be combined
                            with the -f flag.
  -h, --help                Print this message and exit.
"
####################
# Helper functions #
####################
run_test() {
    # Usage: run_test <func> <test>
    local ARGS RESULT
    ARGS="$(get_test_field "${1}" "args")"
    if [[ "${ARGS}" == "null" ]]; then
        eval "${FUNC}"
    else
        eval "${FUNC} ${ARGS}"
    fi
}

compare_results() {
    # Usage: compare_results <test> <test-result>
    diff -u <(echo "${2}") <(get_test_field "${1}" "result.output" | sed 's/null//' ) | tail -n +4
}

should_fail() {
    # Usage: should_fail <test>
    # Returns "false" for tests that shouldn't fail
    # Returns "true" for tests that should fail
    local SHOULD_FAIL="$(get_test_field "${1}" "result.fail")"
    local EXPECTED_RESULT="true"
    if [[ "${SHOULD_FAIL}" == "null" ]] || [[ "${SHOULD_FAIL}" == "false" ]]; then
        # Not expected to fail
        EXPECTED_RESULT="false"
    fi
    echo "${EXPECTED_RESULT}"
}

check_fail() {
    # Usage: check_fail <test> <test-run-exit-code>
    local SHOULD_FAIL="$(should_fail "${TEST}")"
    if [[ "${SHOULD_FAIL}" == "true" ]] && [[ "${2}" -gt 0 ]]; then
        # Expected to fail and did fail
        return 0
    elif [[ "${SHOULD_FAIL}" == "false" ]] && [[ "${2}" -eq 0 ]]; then
        return 0
    fi
    return 1
}

get_test_field() {
    # Usage: get_test_field <test> <field>
    yq e '.'${2} <<< "${1}"
}

echoindent() {
    # Usage: echoindent <string>
    sed 's/^/    /' <<< "${1}"
}

#########################
# Parse arguments/flags #
#########################
SELECTED_FUNCTION=
SELECTED_TEST=
while [[ $# -gt 0 ]]; do
    case $1 in
        -f|--function|--function=*)
            SELECTED_FUNCTION=$(parse_flag "${1}" "${2:-}")
            [[ "${1}" == *"="* ]] || shift
            shift
            ;;

        -t|--test|--test=*)
            SELECTED_TEST=$(parse_flag "${1}" "${2:-}")
            [[ "${1}" == *"="* ]] || shift
            shift
            ;;

        -h|--help)
            echo "${HELP}"
            exit 0
            ;;

        *)
            echo "Unexpected argument '${1}'"
            echo "${USAGE}"
            exit 1
            ;;

    esac
done


########
# Main #
########
# Source 'all' script
. "${REPO_ROOT}/all"

# Iterate over tests. Exit immediately if one fails.
n=1
for FUNCTEST in "${TESTDATA[@]}"; do
    FUNC=$(get_test_field "${FUNCTEST}" key)

    # Check if we've picked a single function
    if [[ -n "${SELECTED_FUNCTION}" ]] && [[ "${FUNC}" != "${SELECTED_FUNCTION}" ]]; then
        continue
    fi

    # Get all
    readarray TESTS < <(get_test_field "${FUNCTEST}" value | yq e '.[]')

    for TEST in "${TESTS[@]}"; do
        # Get test name
        TEST_NAME=$(get_test_field "${TEST}" name)

        # Check if we've picked a single test
        if [[ -n "${SELECTED_TEST}" ]] && [[ "${TEST_NAME}" != "${SELECTED_TEST}" ]]; then
            continue
        fi

        # Run test
        echo -n "Running test ${n} [${FUNC}]: '${TEST_NAME}'. Result: "
        TEST_RESULT=$(run_test "${TEST}" 2>&1)
        EXIT_CODE=$?

        # Check if supposed to fail.
        if ! check_fail "${TEST}" "${EXIT_CODE}"; then
            echo "failed."
            echoindent "Expected to fail: $(should_fail "${TEST}")." 
            echoindent "Exit code: ${EXIT_CODE}"
            echoindent "${TEST_RESULT}"
            exit 1
        fi

        # If not expected to fail, compare results.
        if [[ "$(should_fail "${TEST}")" != "true" ]]; then
            TEST_DIFF=$(compare_results "${TEST}" "${TEST_RESULT}")
            if [[ "$?" -ne 0 ]]; then
                echo "failed."
                echoindent "Result did not match expectation."
                echoindent "${TEST_DIFF}"
                exit 1
            fi
        else
            # If expected to fail and an error string is provided, make sure
            # it matches.
            ERROR_STRING=$(get_test_field "${TEST}" "result.error")
            if [[ "${ERROR_STRING}" != "null" ]]; then
                if ! [[ "${TEST_RESULT}" =~ ${ERROR_STRING} ]]; then
                    echo "failed."
                    echoindent "Observed error message (-) did not match expected (+):"
                    echoindent "$(diff -u <(echo "${TEST_RESULT}") <(echo "${ERROR_STRING}") | tail -n +4)"
                    exit 1
                fi
            fi
        fi

        echo "success."
        n=$((n+1))
    done
done

echo "All tests passed."
